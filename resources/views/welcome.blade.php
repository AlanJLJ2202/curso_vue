<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link rel="stylesheet" href="{{ mix('/css/estilos.css') }}">
    </head>
    <body>

        <div id="app">

            Aplicación de vue 2<br>
            @{{ contador }}<br>
            <button @click="plus()">Plus</button>
            <ul>
                <li v-for="item in nombres" :key="item.id">
                   <span @dblclick="mostrarFormularioEditar(item)">@{{ item.nombre }}</span> - <button @click="eliminarNombre(item)">Eliminar</button>
                </li>
            </ul>
            <button @click="$store.commit('setPanel', 'agregar')">
                Nuevo registro
            </button>
            <div class="agregar modal" :class="{'show': $store.state.panel == 'agregar'}">

            <formulario
            @cerrar="cerrarPanel"
            @guardar="actualizarListado"/>

            </div>

            <div class="editar modal" :class="{'show': $store.state.panel == 'editar'}">

            <formulario
            v-if="registroseleccionado"
            :registro="registroseleccionado"
            @cerrar="cerrarPanel"
            @guardar="actualizarListado"/>

            </div>

        </div>

        <script src="{{ mix('/js/app.js') }}">

        </script>

    </body>
</html>
