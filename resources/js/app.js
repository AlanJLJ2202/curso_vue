const { default: axios } = require('axios');


require('./bootstrap');

window.Vue = require('vue').default;
import Vue from 'vue';
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
import Vuex from 'vuex';
import Formulario from './components/formulario.vue'

Vue.component('v-select', vSelect)
Vue.component('formulario', Formulario);

Vue.use(Vuex);
const store = new Vuex.Store({
    state: {
        panel : null
        //Si el registro es nulo hacer el commit de actualizar el registro en vuex
    },
    mutations: {
        setPanel(state, valor){
            state.panel = valor;
        }
    }
});

const app = new Vue({
    el: '#app',
    store : store,
    data: {
        options: [
            'foo',
            'bar',
            'baz'
          ],
        contador : 0,
        nombres : [],
        nombre : '',
        id_edicion : null,
        registroseleccionado: null
    },
    mounted: function(){
        this.init();
        this.listarNombres();
    },
    methods: {
        plus : function(){
           this.contador++;

           if(this.contador == 10){
            this.contador = 0;
            alert('ya te pasaste pa');
           }

        },
        init : function() {
            this.contador = 1;
        },
        listarNombres : async function(){
            const response = await axios.get('/nombres');
            this.nombres = response.data;
        },
        eliminarNombre :  async function(registro){

            const response = await axios.delete('/nombres/'+registro.id);
            this.nombres.splice(this.nombres.indexOf(registro), 1);
        },
        mostrarFormularioEditar: function (registro){
            this.$store.commit('setPanel', 'editar');
            this.registroseleccionado = Object.assign({}, registro);
        },
        cerrarPanel: function () {
            this.panel = null;
        },
        actualizarListado: function (registro, metodo){

            if(metodo == 'editar'){

                this.nombres.filter( (item, index) => {

                    if(item.id == registro.id){
                        this.nombres.splice(index, 1, registro);
                    }

                });
                this.registroseleccionado = null;

            }else{
                this.nombres.push(registro);

            }
            this.$store.commit('setPanel', null);
        }


    }
});
