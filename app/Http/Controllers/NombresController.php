<?php

namespace App\Http\Controllers;

use App\Models\Nombre;
use Illuminate\Http\Request;

class NombresController extends Controller
{
    
    public function index()
    {
        $nombres = Nombre::orderBy('nombre')->get();
        return response()->json($nombres);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $registro = new Nombre;
        $registro->nombre = $request->input('nombre');
        $registro->save();

        return response()->json($registro);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $registro = Nombre::find($id);
        $registro->nombre = $request->input('nombre');
        $registro->save();

        return response()->json($registro);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Nombre::destroy($id);
        return response()->json('ok');
    }
}
